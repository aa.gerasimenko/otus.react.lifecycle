import { Component, createContext, ReactNode } from "react";
import "./Style.scss";

enum Themes {
  RedBlue,
  YellowGreen,
  BlackWhite,
}

const ThemeObj = {
  [Themes.RedBlue]: "red-blue",
  [Themes.YellowGreen]: "green-yellow",
  [Themes.BlackWhite]: "black-white",
};

const ThemeContext = createContext({ theme: Themes.RedBlue, text: "" });

class Ramka extends Component {
  style = {
    border: "2px dotted black",
    padding: "20px",
  };

  render(): ReactNode {
    return (
      <div style={this.style}>
        <CentralPoint />
      </div>
    );
  }
}

class CentralPoint extends Component {
  style = {
    borderWidth: "2px",
    padding: "10px",
  };

  render(): ReactNode {
    return (
      <ThemeContext.Consumer>
        {(context) => (
          <div className={ThemeObj[context.theme]}>{context.text}</div>
        )}
      </ThemeContext.Consumer>
    );
  }
}

interface ColorButtonProps {
  onClick: (theme: Themes, text: string) => void;
  text: string;
  theme: Themes;
}

class ColorButton extends Component<ColorButtonProps> {
  style = {
    fontSize: "20px",
    margin: "5px",
    padding: "5px",
  };
  render() {
    return (
      <button
        style={this.style}
        className={ThemeObj[this.props.theme]}
        onClick={() => this.props.onClick(this.props.theme, this.props.text)}
      >
        {this.props.text}
      </button>
    );
  }
}

interface ProviderDemoState {
  theme: Themes;
  text: string;
}

export default class ProviderDemo extends Component<{}, ProviderDemoState> {
  constructor(props: {}) {
    super(props);
    this.state = { theme: Themes.BlackWhite, text: "Кликните на кнопку" };
  }
  private a = 1;

  setTheme = (theme: Themes, text: string) => {
    this.a++;
    this.setState({ theme, text });
  };

  getButton = (text: string, theme: Themes) => {
    return <ColorButton text={text} theme={theme} onClick={this.setTheme} />;
  };

  render() {
    return (
      <div>
        Тут значения контекст недоступны
        <ThemeContext.Provider
          value={{ theme: this.state.theme, text: this.state.text }}
        >
          {this.getButton("красно-синий", Themes.RedBlue)}
          {this.getButton("желто-зеленый", Themes.YellowGreen)}
          {this.getButton("черно-белый", Themes.BlackWhite)}
          <Ramka />
        </ThemeContext.Provider>
      </div>
    );
  }
}
