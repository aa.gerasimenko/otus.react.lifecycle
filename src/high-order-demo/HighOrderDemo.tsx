import { Component, ReactNode } from "react";
import "./style.scss";

class Loading extends Component {
  render(): ReactNode {
    return <div className="rainbow"></div>;
  }
}

// КОМПОНЕНТЫ ВЫШЕГО ПОРЯДКА
function withLoading(ToRender: any) {
  return function <T>(props: T & { isLoading: boolean }) {
    const isLoading = props.isLoading;
    if (!isLoading) return <ToRender {...props} />;
    return <Loading />;
  };
}

interface User {
  name: string;
  email: string;
  phone: string;
}

interface HighOrderDemoState {
  users: User[];
  isLoading: boolean;
}

interface TableWithDataProps {
  users: User[];
  color: string;
}
class TableWithData extends Component<TableWithDataProps> {
  render(): ReactNode {
    return (
      <table>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
        </tr>
        <tbody>
          {this.props.users.map((x) => (
            <tr>
              <td style={{ color: this.props.color }}>{x.name}</td>
              <td>{x.email}</td>
              <td>{x.phone}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}

const TableWithLoading = withLoading(TableWithData);

export default class HighOrderDemo extends Component<{}, HighOrderDemoState> {
  constructor(props: any) {
    super(props);
    this.state = { isLoading: false, users: [] };
  }
  componentDidMount(): void {
    this.fetchData();
  }
  fetchData = () => {
    this.setState({ isLoading: true });
    setTimeout(() => {
      fetch(`/users`)
        .then((json) => json.json())
        .then((repos) => {
          this.setState({ isLoading: false, users: repos });
        });
    }, 2000);
  };

  post = () => {
    fetch("/users", {
      method: "POST",
      body: JSON.stringify({ username: "username", password: "" }),
    }).then((x) => console.log(x));
  };

  render() {
    return (
      <>
        <TableWithLoading
          isLoading={this.state.isLoading}
          color="red"
          users={this.state.users}
        />
        <button onClick={this.post}>Отправить</button>
      </>
    );
  }
}
