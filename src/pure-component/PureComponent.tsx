import { Component, PureComponent, ReactNode } from "react";

interface DateTimeDemoProps {
  name: string;
}

class DateTimeDemo extends PureComponent<DateTimeDemoProps> {
  style = {
    fontSize: "20px",
    fontStyle: "bold",
  };
  render(): ReactNode {
    const date = new Date().toString();
    return (
      <span style={{ fontStyle: "italic", color: "red" }}>
        Your name is <span style={this.style}>{this.props.name}</span> and time
        is <span style={this.style}>{date}</span>
      </span>
    );
  }
}

interface PureComponentState {
  name: string;
  password: string;
  show: boolean;
}
export default class PureComponentDemo extends Component<
  {},
  PureComponentState
> {
  constructor(props: {}) {
    super(props);
    this.state = { name: "", password: "", show: true };
  }

  setPassword = (e: any) => {
    this.setState({ password: e.target.value });
  };

  setUsername = (e: any) => {
    this.setState({ name: e.target.value });
  };

  render() {
    return (
      <>
        <label>Юзернейм: </label>
        <input type="text" onChange={this.setUsername} />
        <br />
        <label>Пароль: </label>
        <input type="text" onChange={this.setPassword} />
        <br />
        <DateTimeDemo name={this.state.name} />
        <br />
        <span>
          Юзернейм: {this.state.name} Пароль {this.state.password}
        </span>
      </>
    );
  }
}
