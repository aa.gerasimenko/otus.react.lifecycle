import "./App.css";
import Up from "./up/Up";

function App() {
  return (
    <div className="App">
      <Up />
      {/* <HighOrderDemo /> */}
      {/* <ProviderDemo /> */}
      {/* <PureComponentDemo /> */}
      {/* <LifecycleDemo /> */}
    </div>
  );
}

export default App;
