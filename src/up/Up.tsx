import { Component, PureComponent, ReactNode } from "react";

interface DownProps {
  onSend: (text: string) => void;
}

interface MyState {
  text: string;
}

class Down extends Component<DownProps, MyState> {
  constructor(props: any) {
    super(props);
    this.state = { text: "" };
  }

  setText = (e: any) => {
    this.setState({ text: e.target.value });
  };

  render(): ReactNode {
    return (
      <>
        <label>A: </label>
        <input type="text" onChange={this.setText} />
        <br />
        Введено до отправки: {this.state.text}
        <br />
        <button onClick={() => this.props.onSend(this.state.text)}>
          Отправить
        </button>
      </>
    );
  }
}

export default class Up extends Component<{}, MyState> {
  constructor(props: any) {
    super(props);
    this.state = { text: "" };
  }

  setTextFromDown = (text: string) => {
    this.setState({ text: text });
  };

  render(): ReactNode {
    console.log("ff");
    return (
      <div>
        Вы ввели {this.state.text}
        <br />
        <Down onSend={this.setTextFromDown} />
      </div>
    );
  }
}
