import { Component, PureComponent, ReactNode } from "react";

interface Math {
  a: number;
  b: number;
}

type SummerProps = Math;

interface SummerState {
  data: Math;
}

export class Summer extends Component<SummerProps, SummerState> {
  componentDidMount(): void {
    console.log("SUMMER СМОГ ЗАРЕНДЕРИТЬСЯ");
  }

  componentWillUnmount(): void {
    console.log("%c Ариведерчи", "color:white; background-color: red");
  }
  shouldComponentUpdate(
    nextProps: Readonly<Math>,
    nextState: Readonly<SummerState>,
    nextContext: any
  ): boolean {
    console.log(
      "write",
      nextProps.a + nextProps.b,
      this.props.a + this.props.b
    );
    return nextProps.a + nextProps.b !== this.props.a + this.props.b;
  }

  componentDidUpdate(
    prevProps: Readonly<Math>,
    prevState: Readonly<SummerState>,
    snapshot?: any
  ): void {
    console.log(prevProps);
    console.log(
      "%c Апдейт",
      "color:green; background-color: yellow;font-size:20px"
    );
  }

  constructor(props: SummerProps) {
    super(props);
    this.state = { data: { a: props.a, b: props.b } };
  }

  render(): ReactNode {
    const s = new Date().toString();
    return (
      <span>
        {this.props.a} + {this.props.b} = {this.props.a + this.props.b}
        <br />
        {s}
      </span>
    );
  }
}

interface LifecycleDemoState {
  checked: boolean;
  a: number;
  b: number;
  data: { a: number; b: number };
}

export default class LifecycleDemo extends PureComponent<
  {},
  LifecycleDemoState
> {
  constructor(props: any) {
    super(props);
    this.state = { a: 1, b: 1, data: { a: 1, b: 1 }, checked: true };
  }
  setA = (e: any) => {
    this.setState({ a: parseInt(e.target.value) });
  };

  setB = (e: any) => {
    this.setState({ b: parseInt(e.target.value) });
  };

  setAB = (e: any) => {
    this.setState({ data: { a: this.state.a, b: this.state.b } });
  };

  getSummer = () => {
    if (this.state.checked) {
      const { a, b } = this.state.data;
      return <Summer a={a} b={b} />;
    }

    return null;
  };

  render(): ReactNode {
    const { a, b } = this.state.data;
    return (
      <>
        <label>A: </label>
        <input type="text" onChange={this.setA} />
        <br />
        <label>B: </label>
        <input type="text" onChange={this.setB} />
        <br />
        <button onClick={this.setAB}> Отправить</button>
        <br />
        <button onClick={() => this.setState({ checked: !this.state.checked })}>
          {this.state.checked ? "Выключить" : "Включить"}
        </button>

        <br />
        {/* 
        {this.getSummer()}
        <br /> */}
        {this.state.checked && <Summer a={a} b={b} />}
      </>
    );
  }
}
